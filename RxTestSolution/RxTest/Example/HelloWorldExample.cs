﻿using RxTest.ObserverExample;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace RxTest.Example
{
    class HelloWorldExample
    {
        public void HelloWorldWithObserverExample()
        {
            var observable = Observable.Create<string>(sub =>
           {
               sub.OnNext("Hello world");
               sub.OnCompleted();
               return Disposable.Empty;
           });

            MyConsoleObserver<string> observer = new MyConsoleObserver<string>();
            observable.Subscribe(observer);
        }

        public void ShorterVersion()
        {
            var observable = Observable.Return<string>("Hello world").Subscribe(
                s => Console.WriteLine($"Item received: {s}")
                );
        }
    }
}