﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using RxTest.Extensions;

namespace RxTest
{
    class TransformationExamples
    {
        public void SelectWithTheSameType()
        {
            var source = Observable.Range(0, 5);
            source.Select(i => i + 3).Dump("+3");
        }

        public void SelectWithDifferentType()
        {
            var source = Observable.Range(0, 5);
            source.Select(i => (char)(i+64)).Dump("toChar");
        }

        public void SelectWithIndex()
        {
            var source = Observable.Range(0, 5);
            source.Select((idx, i) => new { Character = (char)(i + 64), Index = idx }).Dump("toChar");
        }

        public void Cast()
        {
            var objects = new Subject<object>();
            objects.Cast<int>().Dump("cast");

            objects.OnNext(1);
            objects.OnNext(2);
            objects.OnNext(3);

            objects.OnCompleted();
        }

        public void CastWithError()
        {
            var objects = new Subject<object>();
            objects.Cast<int>().Dump("cast");

            objects.OnNext(1);
            objects.OnNext(2);
            objects.OnNext("3");

            objects.OnCompleted();
        }

        public void OfTypeIgnoreError()
        {
            var objects = new Subject<object>();
            objects.OfType<int>().Dump("ofType");

            objects.OnNext(1);
            objects.OnNext(2);
            objects.OnNext("3");
            objects.OnNext(4);

            objects.OnCompleted();
        }

        //From one select many, or maybe better is from one select zero or more.
        public void SelectManyExample()
        {
            Observable.Return(3)
                .SelectMany(i => Observable.Range(1, i))
                .Dump("SelectMany");
        }

        public void SelectManyExample2()
        {
            //[1][1,2][1,2,3]
            Observable.Range(1, 3)
                .SelectMany(i => Observable.Range(1, i))
                .Dump("SelectMany2");
        }

        public void SelectManyExample3()
        {
            Func<int, char> letter = i => (char)(i + 64);
            Observable.Return(1)
                .SelectMany(i => Observable.Return(letter(i))).Dump("SelectMany3");            
        }
        
        //[1,2,3] -> [A,B,C]
        public void SelectManyExample4()
        {
            Func<int, char> letter = i => (char)(i + 64);
            Observable.Range(1, 3)
                .SelectMany(i => Observable.Return(letter(i))).Dump("SelectMany4");
        }

        public void SelectManyExample5()
        {
            Func<int, char> letter = i => (char)(i + 64);
            Observable.Range(1, 30)
                .SelectMany(i =>
                {
                    if (0 < i && i < 27)
                    {
                        return Observable.Return(letter(i));
                    }
                    else
                    {
                        return Observable.Empty<char>();
                    }
                }).Dump("Observable 5");
        }
    }
}
