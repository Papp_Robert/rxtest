﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using RxTest.Example;
using RxTest.ExceptionHandling;
using RxTest.HotAndColdObservables;
using RxTest.ObservableFactory;
using RxTest.ObserverExample;
using RxTest.Sequences;

namespace RxTest
{
    class Program
    {
        static void Main(string[] args)
        {
            HelloWorldExample helloWorldExample = new HelloWorldExample();
            //helloWorldExample.HelloWorldWithObserverExample();
            //helloWorldExample.ShorterVersion();

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            MyConsoleObserver<int> intConsoleObserver = new MyConsoleObserver<int>();
            MyConsoleObserver<long> longConsoleObserver = new MyConsoleObserver<long>();

            ObservableFactoryExample example = new ObservableFactoryExample();

            var subject = new Subject<string>();
            //subject.OnNext("a");

            ////At this point we subscribe to the subject.
            //WriteSequenceToConsole(subject);
            //subject.OnNext("b");
            //subject.OnNext("c");            

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            ReplaySubjectExample rse = new ReplaySubjectExample();
            //rse.ReplaySubjectBufferExample();            

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            BehaviorSubjectExample bse = new BehaviorSubjectExample();
            bse.BehaviorSubjectExample2();

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            ObservableFactoryExample factoryExample = new ObservableFactoryExample();
            var observable = factoryExample.SetUp();

            MyConsoleObserver<string> myConsoleObserver = new MyConsoleObserver<string>();
            //observable.Subscribe(myConsoleObserver);            

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            /*
            var range = Observable.Range(10, 15);
            range.Subscribe(Console.WriteLine, () => Console.WriteLine("Completed"));
            */

            //EQUAL To this

            /*
            ObservableFactoryExample example = new ObservableFactoryExample();
            var observer = example.Range(0, 15);

            observer.Subscribe(intConsoleObserver);
            */

            /*
            var observer = example.Interval(new TimeSpan(0, 0, 1));
            observer.Subscribe(longConsoleObserver);
            */

            /*
            var numbers = new MySequenceOfNumbers();
            var observer = new MyConsoleObserver<int>();
            numbers.Subscribe(observer);
            */

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            /*
            TimerWithObservable timerWithObservable = new TimerWithObservable();
            var observable = timerWithObservable.Timer(new TimeSpan(2000));

            MyConsoleObserver<long> myConsoleObserver = new MyConsoleObserver<long>();
            observable.Subscribe(myConsoleObserver);
            */

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            //StartAction();
            //StartFunc();

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            SequenceExamples sequenceExamples = new SequenceExamples();
            //sequenceExamples.SkipWhile();
            //sequenceExamples.TakeWhile();
            //sequenceExamples.SkipLast();
            //sequenceExamples.TakeLast();
            //sequenceExamples.SkipUntil();
            //sequenceExamples.TakeUntil();
            //sequenceExamples.Any();

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            AggregateExamples aggregateExamples = new AggregateExamples();
            //aggregateExamples.Count();
            //aggregateExamples.Max();
            //aggregateExamples.Scan();
            //aggregateExamples.ActMax();
            //aggregateExamples.GroupBy();

            TransformationExamples transformationExamples = new TransformationExamples();
            //transformationExamples.SelectWithTheSameType();
            //transformationExamples.SelectWithDifferentType();
            //transformationExamples.SelectWithIndex();
            //transformationExamples.Cast();
            //transformationExamples.CastWithError();
            //transformationExamples.OfTypeIgnoreError();
            //transformationExamples.SelectManyExample();
            //transformationExamples.SelectManyExample2();
            //transformationExamples.SelectManyExample3();
            //transformationExamples.SelectManyExample4();
            //transformationExamples.SelectManyExample5();

            ///////////////////////////////////////////////////
            ///////////////////////////////////////////////////

            MonadExample monadExample = new MonadExample();
            //monadExample.ForEach();
            //monadExample.ToEnumerable();
            //monadExample.ToArray();
            //monadExample.ToTask();

            HandleExceptions handleExceptions = new HandleExceptions();
            //handleExceptions.TryBlock();
            //handleExceptions.Finally();
            //handleExceptions.Finally2();
            //handleExceptions.Concat();
            //handleExceptions.Repeat();
            //handleExceptions.StartsWith();
            //handleExceptions.Amb();
            //handleExceptions.Merge();
            //handleExceptions.CombineLatest();
            //handleExceptions.Zip();
            //handleExceptions.Zip2();
            //handleExceptions.AndThenWhen();
            //handleExceptions.AndThenWhen2();

            TimeShiftedSequencesExample timeShiftedSequencesExample = new TimeShiftedSequencesExample();
            //timeShiftedSequencesExample.Buffer();
            //timeShiftedSequencesExample.BufferWithSkip();
            //timeShiftedSequencesExample.BufferWithOverlapping();
            //timeShiftedSequencesExample.Delay();
            //timeShiftedSequencesExample.Sample();
            //timeShiftedSequencesExample.Timeout();
            //timeShiftedSequencesExample.Timeout2();

            HotAndColdObservablesExample hotAndColdObservables = new HotAndColdObservablesExample();
            //hotAndColdObservables.PublishAndConnect();
            //hotAndColdObservables.PublishAndConnect2();
            //hotAndColdObservables.Disposal();
            //hotAndColdObservables.Disposal2();
            //hotAndColdObservables.RefCount();
            //hotAndColdObservables.PublishLast();
            //hotAndColdObservables.Replay();

            WindowExample windowExample = new WindowExample();
            //windowExample.TryWindowExample();
            //windowExample.TryWindowExample2();

            Console.ReadLine();
        }

        static void WriteSequenceToConsole(IObservable<string> sequence)
        {
            sequence.Subscribe(Console.WriteLine);
        }

        static void StartAction()
        {
            var start = Observable.Start(() =>
           {
               Console.Write("Working away");
               for (int i = 0; i < 10; i++)
               {
                   Console.Write(".");
               }
           });

            start.Subscribe(
                unit => Console.WriteLine("Unit published"),
                () => Console.WriteLine("Action completed"));
        }

        static void StartFunc()
        {
            var start = Observable.Start(() =>
            {
                Console.Write("Working away");
                for (int i = 0; i < 10; i++)
                {
                    Console.Write(".");
                }
                return "Published value";
            });

            start.Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("Action completed"));
        }
    }
}
