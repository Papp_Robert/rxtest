﻿using RxTest.Extensions;
using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace RxTest.ExceptionHandling
{
    public class HandleExceptions
    {
        public void TryBlock()
        {
            var source = new Subject<int>();

            var result = source.Catch<int, TimeoutException>(tx => Observable.Return(-1));
            result.Dump("Catch");

            source.OnNext(1);
            source.OnNext(2);
            source.OnError(new ArgumentException("fail!"));
        }

        public void Finally()
        {
            var source = new Subject<int>();
            var result = source.Finally(() => Console.WriteLine("Finally action ran"));

            result.Dump("Finally");

            source.OnNext(1);
            source.OnNext(2);
            source.OnNext(3);
            source.OnCompleted();
        }

        public void Finally2()
        {
            var source = new Subject<int>();
            var result = source.Finally(() => Console.WriteLine("Finally action ran"));

            var subscription = result.Subscribe(Console.WriteLine, Console.WriteLine, () => Console.WriteLine("Completed"));

            source.OnNext(1);
            source.OnNext(2);
            source.OnNext(3);
            source.Retry(2);
            source.OnError(new Exception("Failed"));

            //subscription.Dispose();
        }

        public void Concat()
        {
            var s1 = Observable.Range(0, 3);
            var s2 = Observable.Range(5, 5);

            s1.Concat(s2).Subscribe(Console.WriteLine);
        }

        public void Repeat()
        {
            var source = Observable.Range(0, 3);
            var result = source.Repeat(3);

            result.Subscribe(Console.WriteLine, () => Console.WriteLine("Completed"));
        }

        public void StartsWith()
        {
            var source = Observable.Range(0, 3);
            var result = source.StartWith(-3, -2, -1);

            result.Subscribe(Console.WriteLine, () => Console.WriteLine("Completed"));
        }

        public void Amb()
        {
            var s1 = new Subject<int>();
            var s2 = new Subject<int>();
            var s3 = new Subject<int>();

            var result = Observable.Amb(s1, s2, s3);
            result.Subscribe(Console.WriteLine, () => Console.WriteLine("Completed"));
            //s1.OnNext(1);
            s2.OnNext(2);
            s3.OnNext(3);

            s1.OnNext(1);
            s2.OnNext(2);
            s3.OnNext(3);

            s1.OnCompleted();
            s2.OnCompleted();
            s3.OnCompleted();
        }

        //Race condition
        public void Merge()
        {
            var s1 = Observable.Interval(TimeSpan.FromMilliseconds(250)).Take(3); //250-500-750

            var s2 = Observable.Interval(TimeSpan.FromMilliseconds(150)).Take(5) //150,300,450,600,750
                .Select(i => i + 100);

            s1.Merge(s2)
                .Subscribe(Console.WriteLine,
                () => Console.WriteLine("Completed"));
        }

        public void CombineLatest()
        {
            var s1 = Observable.Range(0, 5);
            var s2 = new Subject<string>();

            s2.OnNext("a");
            s2.OnNext("dd");
            s2.OnNext("eee");

            var pairs = s1.CombineLatest(s2, (s, i) => s >= 0 && i.Length > 1);

            pairs.Dump("CombineLatest");
        }

        public void Zip()
        {
            var nums = Observable.Interval(TimeSpan.FromMilliseconds(250))
                .Take(3);

            var chars = Observable.Interval(TimeSpan.FromMilliseconds(150))
                .Take(6)
                .Select(i => Char.ConvertFromUtf32((int)i + 97));

            nums.Zip(chars, (lhs, rhs) => new { Left = lhs, Right = rhs }).Dump("Zip");
        }

        public void Zip2()
        {
            var one = Observable.Interval(TimeSpan.FromSeconds(1)).Take(5);
            var two = Observable.Interval(TimeSpan.FromMilliseconds(250)).Take(10);
            var three = Observable.Interval(TimeSpan.FromMilliseconds(150)).Take(14);

            var zippedSequence = one.Zip(two, (lhs, rhs) => new { One = lhs, Two = rhs }).Zip(three, (lhs, rhs) => new { One = lhs, Two = lhs.Two, Three = rhs });

            zippedSequence.Subscribe(Console.WriteLine, () => Console.WriteLine("Completed"));
        }

        public void AndThenWhen()
        {
            var one = Observable.Interval(TimeSpan.FromSeconds(1)).Take(5);
            var two = Observable.Interval(TimeSpan.FromMilliseconds(250)).Take(10);
            var three = Observable.Interval(TimeSpan.FromMilliseconds(150)).Take(14);

            var pattern = one.And(two).And(three);
            var plan = pattern.Then(
                (first, second, third) => new { One = first, Two = second, Three = third });

            var zippedSequence = Observable.When(plan);

            zippedSequence.Subscribe(Console.WriteLine, () => Console.WriteLine("Completed"));
        }

        public void AndThenWhen2()
        {
            var one = Observable.Interval(TimeSpan.FromSeconds(1)).Take(5);
            var two = Observable.Interval(TimeSpan.FromMilliseconds(250)).Take(10);
            var three = Observable.Interval(TimeSpan.FromMilliseconds(150)).Take(14);

            var pattern = one.And(two).And(three);
            var plan = pattern.Then(
                (first, second, third) => new { One = first, Two = second, Three = third });

            var zippedSequence = Observable.When(one.And(two).And(three).Then(
                (first, second, third) => 
                new { One = first, Two = second, Three = third }));

            zippedSequence.Subscribe(Console.WriteLine, () => Console.WriteLine("Completed"));
        }

    }
}
