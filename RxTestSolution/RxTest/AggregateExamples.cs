﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using RxTest.Extensions;

namespace RxTest
{
    class AggregateExamples
    {

        public void Count()
        {
            var numbers = Observable.Range(0, 3);
            numbers.Dump("numbers");
            numbers.Count().Dump("count");
        }

        public void Max()
        {
            var numbers = Observable.Range(0, 10);
            //numbers.Aggregate((max, currentValue) => Comparer<int>.Default.Compare(max, currentValue) > 0 ? max : currentValue).Dump("max") ;
            numbers.Aggregate((max, currentValue) => max > currentValue ? max : currentValue).Dump("max");
        }

        public void Scan()
        {
            var numbers = new Subject<int>();
            var scan = numbers.Scan(0, (acc, current) => acc + current);

            numbers.Dump("numbers");
            scan.Dump("scan");

            numbers.OnNext(1);
            numbers.OnNext(2);
            numbers.OnNext(4);
            numbers.OnCompleted();
        }

        public void ActMax()
        {
            var numbers = new Subject<int>();

            int max = 0;
            var scan = numbers.Scan((x, y) => x > y ? x : y);

            numbers.Dump("numbers");
            scan.Dump("scan");

            numbers.OnNext(1);
            numbers.OnNext(2);
            numbers.OnNext(4);
            numbers.OnNext(1);
            numbers.OnNext(2);


            numbers.OnCompleted();
        }

        public void GroupBy()
        {
            var source = Observable.Interval(TimeSpan.FromSeconds(0.1)).Take(10);

            var group = source.GroupBy(i => i % 3);
            group.Subscribe(
                grp =>
                grp.Min().Subscribe(
                    minValue => Console.WriteLine("{0} min value = {1}", grp.Key, minValue)),
                () => Console.WriteLine("Completed")
            );
        }
    }
}
