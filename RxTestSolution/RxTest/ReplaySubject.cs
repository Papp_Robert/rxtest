﻿using System;
using System.Reactive.Subjects;

namespace RxTest
{
    class ReplaySubjectExample
    {

        public void ReplaySubjectBufferExample()
        {
            var bufferSize = 2;
            var subject = new ReplaySubject<string>(bufferSize);

            subject.OnNext("a");
            subject.OnNext("b");
            subject.OnNext("c");

            //subject.Subscribe(Console.WriteLine);
            subject.Subscribe((x) => Console.WriteLine("Value: {0}", x.ToString()));

            subject.OnNext("d");
        }
    }
}
