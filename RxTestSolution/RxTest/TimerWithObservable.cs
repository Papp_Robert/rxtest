﻿using System;
using System.Reactive.Linq;

namespace RxTest
{
    public class TimerWithObservable
    {

        public void SetTimer()
        {
            var timer = Observable.Timer(TimeSpan.FromSeconds(1));

            timer.Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("completed"));
        }

        public IObservable<long> Timer(TimeSpan dueTime)
        {
            return Observable.Generate(
                0L,
                i => i < 1,
                i => i + 1,
                i => i,
                i => dueTime);
        }

        public IObservable<long> Timer(TimeSpan dueTime, TimeSpan period)
        {
            return Observable.Generate(
                0L,
                i => true,
                i => i + 1,
                i => i,
                i => i == 0 ? dueTime : period);
        }
    }
}
