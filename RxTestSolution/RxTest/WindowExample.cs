﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace RxTest
{
    class WindowExample
    {
        public void TryWindowExample()
        {
            var windowId = 0;
            var source = Observable.Interval(TimeSpan.FromSeconds(1)).Take(10);
            source.Window(3)
                .Subscribe(window =>
                {
                    var id = windowId++;
                    Console.WriteLine("Starting new window");
                    var windowName = "Window" + windowId;

                    window.Subscribe(
                        value => Console.WriteLine("{0} : {1}", windowName, value),
                        ex => Console.WriteLine("{0} : {1}", windowName, ex),
                        () => Console.WriteLine("{0} Completed", windowName)
                        );
                },
                () => Console.WriteLine("Completed"));
        }

        public void TryWindowExample2()
        {
            var windowId = 0;
            var source = Observable.Interval(TimeSpan.FromSeconds(1)).Take(10);
            var closer = new Subject<Unit>();
            
            source.Window(() => closer)
                .Subscribe(window =>
                {
                    var id = windowId++;
                    Console.WriteLine("Starting new window");
                    var windowName = "Window" + windowId;

                    window.Subscribe(
                        value => Console.WriteLine("{0} : {1}", windowName, value),
                        ex => Console.WriteLine("{0} : {1}", windowName, ex),
                        () => Console.WriteLine("{0} Completed", windowName)
                        );
                },
                () => Console.WriteLine("Completed"));
            var input = "";
            while(input !="exit")
            {
                input = Console.ReadLine();
                closer.OnNext(Unit.Default);
            }
        }
    }
}
 