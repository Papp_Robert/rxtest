﻿using System;
using System.Reactive.Subjects;

namespace RxTest
{
    class BehaviorSubjectExample
    {
        public void BehaviorSubjectExample2()
        {
            var subject = new BehaviorSubject<string>("a");

            subject.OnNext("b");
            subject.Subscribe(Console.WriteLine);
        }
    }
}
