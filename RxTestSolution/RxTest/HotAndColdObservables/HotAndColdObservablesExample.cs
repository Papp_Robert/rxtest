﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;

namespace RxTest.HotAndColdObservables
{
    class HotAndColdObservablesExample
    {
        //COLD: Typical, calling the method does nothing, it just returns an IObservable, to get the data we need to subscribe on it.
        //HOT: PRoduces data if theere is no subscription also. UI events and Subjects are classic examples.

        public void PublishAndConnect()
        {
            var period = TimeSpan.FromSeconds(1);
            var observable = Observable.Interval(period).Publish();
            observable.Connect();

            observable.Subscribe(
                i => Console.WriteLine("first subscription : {0}", i));

            Thread.Sleep(period);

            observable.Subscribe(
               i => Console.WriteLine("second subscription : {0}", i));
        }

        public void PublishAndConnect2()
        {
            var period = TimeSpan.FromSeconds(1);
            var observable = Observable.Interval(period).Publish();
            observable.Subscribe(
                i => Console.WriteLine("first subscription : {0}", i));

            Thread.Sleep(period);

            observable.Subscribe(
               i => Console.WriteLine("second subscription : {0}", i));
            observable.Connect();
        }

        public void Disposal()
        {
            var period = TimeSpan.FromSeconds(1);
            var observable = Observable.Interval(period).Publish();
            observable.Subscribe(
                i => Console.WriteLine("subscription : {0}", i));
            var exit = false;
            while (!exit)
            {
                Console.WriteLine("Press ener to connect, esc to exit.");
                var key = Console.ReadKey(true);

                if(key.Key == ConsoleKey.Enter)
                {
                    var connection = observable.Connect();
                    Console.WriteLine("Press any key to dispose of connection");
                    Console.ReadKey();
                    connection.Dispose();
                }
                if(key.Key == ConsoleKey.Escape)
                {
                    exit = true;
                }                
            }
        }


        public void Disposal2()
        {
            var period = TimeSpan.FromSeconds(1);
            var observable = Observable.Interval(period)
                .Do(l => Console.WriteLine("Publishing {0}", l)).Publish();
            observable.Connect();
            Console.WriteLine("Press any key to subscribe");

            Console.ReadKey();
            var subscription = observable.Subscribe(i => Console.WriteLine("subscription: {0}", i));
            Console.WriteLine("Press any key to unsubscribe");
            Console.ReadKey();
            subscription.Dispose();
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        public void RefCount()
        {
            var period = TimeSpan.FromSeconds(1);
            var observable = Observable.Interval(period)
                .Do(l => Console.WriteLine("Publishing {0}", l))
                .Publish()
                .RefCount();            
            Console.WriteLine("Press any key to subscribe");
            Console.ReadKey();
            var subscription = observable.Subscribe(i => Console.WriteLine("subscription: {0}", i));
            Console.WriteLine("Press any key to unsubscribe");
            Console.ReadKey();
            subscription.Dispose();
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        public void PublishLast()
        {
            var period = TimeSpan.FromSeconds(1);
            var observable = Observable.Interval(period)
            .Take(5)
            .Do(l => Console.WriteLine("Publishing {0}", l)) //side effect to show it is running
            .PublishLast();
            observable.Connect();
            Console.WriteLine("Press any key to subscribe");
            Console.ReadKey();
            var subscription = observable.Subscribe(i => Console.WriteLine("subscription : {0}", i));
            Console.WriteLine("Press any key to unsubscribe.");
            Console.ReadKey();
            subscription.Dispose();
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        public void Replay()
        {
            var period = TimeSpan.FromSeconds(1);
            var hot = Observable.Interval(period)
                .Take(3)
                .Publish();
            hot.Connect();
            Thread.Sleep(period);

            var observable = hot.Replay();
            observable.Connect();

            observable.Subscribe(i => Console.WriteLine("first subscription: {0}", i));
            Thread.Sleep(period);

            observable.Subscribe(i => Console.WriteLine("second subscription: {0}", i));
            Console.ReadKey();

            observable.Subscribe(i => Console.WriteLine("third subscription: {0}", i));
            Console.ReadKey();
        }

        public void Multicast()
        {
            var period = TimeSpan.FromSeconds(1);
            //var observable = Observable.Interval(period).Publish();
            var observable = Observable.Interval(period);
            var shared = new Subject<long>();

            shared.Subscribe(i => Console.WriteLine("first subscription : {0}", i));
            observable.Subscribe(shared);   //'Connect' the observable.

            Thread.Sleep(period);
            Thread.Sleep(period);

            shared.Subscribe(i => Console.WriteLine("second subscription : {0}", i));

        }
    }
}
