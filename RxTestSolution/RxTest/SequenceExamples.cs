﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace RxTest
{
    public class SequenceExamples
    {

        public void SkipWhile()
        {
            var subject = new Subject<int>();

            subject.SkipWhile(i => i < 4)
                .Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("Completed")
                );

            subject.OnNext(1);
            subject.OnNext(2);
            subject.OnNext(3);
            subject.OnNext(4);
            subject.OnNext(3);
            subject.OnNext(2);
            subject.OnNext(1);
            subject.OnNext(0);

            subject.OnCompleted();
        }


        public void TakeWhile()
        {
            var subject = new Subject<int>();

            subject.TakeWhile(i => i < 4)
                .Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("Completed")
                );

            subject.OnNext(1);
            subject.OnNext(2);
            subject.OnNext(3);
            subject.OnNext(4);
            subject.OnNext(3);
            subject.OnNext(2);
            subject.OnNext(1);
            subject.OnNext(0);

            subject.OnCompleted();
        }

        public void SkipLast()
        {
            var subject = new Subject<int>();

            subject.SkipLast(2)
                .Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("Completed")
                );

            Console.WriteLine("Pushing 1");
            subject.OnNext(1);

            Console.WriteLine("Pushing 2");
            subject.OnNext(2);

            Console.WriteLine("Pushing 3");
            subject.OnNext(3);

            Console.WriteLine("Pushing 4");
            subject.OnNext(4);

            Console.WriteLine("Pushing 3");
            subject.OnNext(3);

            Console.WriteLine("Pushing 2");
            subject.OnNext(2);

            Console.WriteLine("Pushing 1");
            subject.OnNext(1);

            Console.WriteLine("Pushing 0");
            subject.OnNext(0);

            subject.OnCompleted();
        }

        public void TakeLast()
        {
            var subject = new Subject<int>();

            subject.TakeLast(2)
                .Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("Completed")
                );

            Console.WriteLine("Pushing 1");
            subject.OnNext(1);

            Console.WriteLine("Pushing 2");
            subject.OnNext(2);

            Console.WriteLine("Pushing 3");
            subject.OnNext(3);

            Console.WriteLine("Pushing 4");
            subject.OnNext(4);

            Console.WriteLine("Pushing 3");
            subject.OnNext(3);

            Console.WriteLine("Pushing 2");
            subject.OnNext(2);

            Console.WriteLine("Pushing 1");
            subject.OnNext(1);

            Console.WriteLine("Pushing 0");
            subject.OnNext(0);

            subject.OnCompleted();
        }

        public void SkipUntil()
        {
            var subject = new Subject<int>();
            var otherSubject = new Subject<Unit>();

            subject
                .SkipUntil(otherSubject)
                .Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("Completed"));

            subject.OnNext(1);
            subject.OnNext(2);
            subject.OnNext(3);

            otherSubject.OnNext(Unit.Default);

            subject.OnNext(4);
            subject.OnNext(5);
            subject.OnNext(6);
            subject.OnNext(7);
            subject.OnNext(8);

            subject.OnCompleted();
        }

        public void TakeUntil()
        {
            var subject = new Subject<int>();
            var otherSubject = new Subject<Unit>();

            subject
                .TakeUntil(otherSubject)
                .Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("Completed"));

            subject.OnNext(1);
            subject.OnNext(2);
            subject.OnNext(3);

            otherSubject.OnNext(Unit.Default);

            subject.OnNext(4);
            subject.OnNext(5);
            subject.OnNext(6);
            subject.OnNext(7);
            subject.OnNext(8);

            subject.OnCompleted();
        }

        public void Any()
        {
            var subject = new Subject<int>();

            subject
            .Subscribe
            (
                Console.WriteLine,
                () => Console.WriteLine("Completed")
            );

            var any = subject.Any(x => x > 2);
            any.Subscribe(
                (b) => Console.WriteLine("has value {0}" , b), 
                () => Console.WriteLine(" Any Completed")
            );

            subject.OnNext(1);
            subject.OnNext(2);
            subject.OnNext(3);
        }
    }
}
