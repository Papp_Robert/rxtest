﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RxTest
{
    public delegate int Calculate(int x, int y);

    class DelegateActionExample
    {
        public DelegateActionExample()
        {
            Calculate calculate = AddTwoNumbers;

            int twoPlusThree = calculate(2, 3);

            Calculate del = delegate (int x, int y) { return x + y; };

            Calculate del2 = (x, y) =>
            {
                return x + y;
            };


            Func<int, int, int> delegateWithFunction = AddTwoNumbers;
        }

        public int AddTwoNumbers(int x, int y)
        {
            return x + y;
        }
    }
}
