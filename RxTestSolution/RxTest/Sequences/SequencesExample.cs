﻿using RxTest.Extensions;
using System;
using System.Reactive.Linq;

namespace RxTest.Sequences
{
    class TimeShiftedSequencesExample
    {
        public void Buffer()
        {
            var idealBatchSize = 15;
            var maxTimeDelay = TimeSpan.FromSeconds(3);
            var source = Observable.Interval(TimeSpan.FromSeconds(1)).Take(10)
                .Concat(Observable.Interval(TimeSpan.FromSeconds(0.01)).Take(100));

            source.Buffer(maxTimeDelay, idealBatchSize)
                .Subscribe(
                buffer => Console.WriteLine("Buffer of {1} @ {0}", DateTime.Now, buffer.Count),
                () => Console.WriteLine("Completed"));
        }

        public void BufferWithSkip()
        {
            var source = Observable.Interval(TimeSpan.FromSeconds(1)).Take(10);
            source.Buffer(3,1)
                .Subscribe(
                buffer =>
                {
                    Console.WriteLine("-- Buffered values");
                    foreach (var value in buffer)
                    {
                        Console.WriteLine(value);
                    }

                }, () => Console.WriteLine("Completed")
                );
        }

        public void BufferWithOverlapping()
        {
            var source = Observable.Interval(TimeSpan.FromSeconds(1)).Take(10);
            var overlapped = source.Buffer(
                TimeSpan.FromSeconds(3), TimeSpan.FromSeconds(1));

            var standard = source.Buffer(
                TimeSpan.FromSeconds(3), TimeSpan.FromSeconds(3));

            var skipped = source.Buffer(
                TimeSpan.FromSeconds(3), TimeSpan.FromSeconds(5));

            overlapped.Subscribe(
                buffer =>
                {
                    Console.WriteLine("-- Overlapped Buffered values");
                    foreach (var value in buffer)
                    {
                        Console.WriteLine(value);
                    }

                }, () => Console.WriteLine("Overlapped Completed")
                );
            standard.Subscribe(
                buffer =>
                {
                    Console.WriteLine("-- Standard Buffered values");
                    foreach (var value in buffer)
                    {
                        Console.WriteLine(value);
                    }

                }, () => Console.WriteLine(" Standard Completed")
                );
            skipped.Subscribe(
                buffer =>
                {
                    Console.WriteLine("-- Skipped Buffered values");
                    foreach (var value in buffer)
                    {
                        Console.WriteLine(value);
                    }

                }, () => Console.WriteLine("Skipped Completed")
                );
        }

        public void Delay()
        {
            var source = Observable.Interval(TimeSpan.FromSeconds(1))
                .Take(5)
                .Timestamp();

            var delay = source.Delay(TimeSpan.FromSeconds(2));

            source.Subscribe(
                value => Console.WriteLine("source : {0}", value),
                () => Console.WriteLine("source completed")
                );

            delay.Subscribe(
                value => Console.WriteLine("delay : {0}", value),
                () => Console.WriteLine("delay completed")
                );
        }

        public void Sample()
        {
            var interval = Observable.Interval(
                TimeSpan.FromMilliseconds(150));

            interval.Sample(TimeSpan.FromSeconds(1)).Subscribe(Console.WriteLine);
        }

        public void Timeout()
        {
            var source = Observable.Interval(TimeSpan.FromMilliseconds(100)).Take(10)
                .Concat(Observable.Interval(TimeSpan.FromSeconds(2)));

            var timeout = source.Timeout(TimeSpan.FromSeconds(1));

            timeout.Subscribe(
                Console.WriteLine,
                (i)=> Console.WriteLine(i),
                () => Console.WriteLine("Completed")
                );
        }

        public void Timeout2()
        {
            var dueDate = DateTimeOffset.UtcNow.AddSeconds(4);
            var source = Observable.Interval(TimeSpan.FromSeconds(1));

            var timeout = source.Timeout(dueDate);

            timeout.Subscribe(
                Console.WriteLine,
                (i) => Console.WriteLine(i),
                () => Console.WriteLine("Completed")
                );
        }
    }
}
