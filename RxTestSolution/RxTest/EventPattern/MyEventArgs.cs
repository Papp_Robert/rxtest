﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RxTest.EventPattern
{
    public class MyEventArgs : EventArgs
    {
        public long Value { get; private set; }

        public MyEventArgs(long value)
        {
            this.Value = value;
        }
    }
}
