﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RxTest.EventPattern
{
    public interface IEventPatternSource<TEventArgs> where TEventArgs :EventArgs
    {
        event EventHandler<TEventArgs> OnNext;
    }
}
