﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace RxTest.ObservableFactory
{
    public class ObservableFactoryExample
    {
        public IObservable<string> SetUp()
        {
            return Observable.Return("Value");
        }

        public IObservable<T> Never<T>()
        {
            return Observable.Create<T>(
                (IObserver<T> observer) =>
            {
                return Disposable.Empty;            
            });
        }

        public IObservable<T> Empty<T>()
        {
            return Observable.Create<T>(
                (IObserver<T> observer) =>
                {
                    observer.OnCompleted();
                    return Disposable.Empty;
                });
        }

        public IObservable<T> Return<T>(T value)
        {
            return Observable.Create<T> (
                (IObserver<T> observer) =>
                {
                    observer.OnNext(value);
                    observer.OnCompleted();

                    return Disposable.Empty;
                });
        }

        public IObservable<T> Throw<T>(T value)
        {
            return Observable.Create<T>(
                (IObserver<T> observer) =>
                {
                    observer.OnError(new Exception());

                    return Disposable.Empty;
                });
        }

        public IObservable<int> Range(int start, int count)
        {
            var max = start + count;
            return Observable.Generate(
                start,
                i => i < max,
                i => i + 1,
                i => i
                );
        }

        public IObservable<long> Interval(TimeSpan period)
        {
            return Observable.Timer(period, period);
        }
    }
}
