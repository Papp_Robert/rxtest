﻿using RxTest.EventPattern;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;

namespace RxTest
{
    public class MonadExample
    {
        public void ForEach()
        {
            var source = Observable.Interval(
                TimeSpan.FromSeconds(1))
                .Take(5);

            source.ForEach(i => Console.WriteLine("received {0}", i));
            Console.WriteLine("Completed foreach {0}", DateTime.Now);

            source.Subscribe(i => Console.WriteLine("Received in subscribe {0}", i));
            Console.WriteLine("Completed subscribe {0}", DateTime.Now);
        }

        //only block if an element is not available, when try to do something with it.
        public void ToEnumerable()
        {
            var period = TimeSpan.FromMilliseconds(200);
            var source = Observable.Timer(TimeSpan.Zero, period).Take(5);

            var result = source.ToEnumerable();

            foreach (var value in result)
            {
                Console.WriteLine(value);
            }
            Console.WriteLine("Done");
        }

        public void ToArray()
        {
            var period = TimeSpan.FromMilliseconds(200);
            var source = Observable.Timer(TimeSpan.Zero, period).Take(5);

            var result = source.ToArray();

            result.Subscribe(arr =>
            {
                Console.WriteLine("Received array");

                foreach (var value in arr)
                {
                    Console.WriteLine(value);
                }
            },
            () => Console.WriteLine("Completed"));
            Console.WriteLine("Subscribed");
        }

        public void ToTask()
        {
            var source = Observable.Interval(
                TimeSpan.FromSeconds(1)).Take(5);

            var result = source.ToTask();
            Console.WriteLine(result.Result);
        }

        public void ToEvent()
        {
            var source = Observable.Interval(
                TimeSpan.FromSeconds(1)).Take(5);

            var result = source.ToEvent();
            result.OnNext += val => Console.WriteLine(val);
        }
    }
}
