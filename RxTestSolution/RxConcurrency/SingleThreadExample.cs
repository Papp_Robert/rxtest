﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;

namespace RxConcurrency
{
    class SingleThreadExample
    {

        public void SingleThread()
        {
            Console.WriteLine("Starting on threadId {0}", Thread.CurrentThread.ManagedThreadId);
            var subject = new Subject<object>();

            subject.Subscribe(
                o => Console.WriteLine("Received {1} on threadId: {0}", Thread.CurrentThread.ManagedThreadId, o));

            ParameterizedThreadStart notify = obj =>
            {
                Console.WriteLine("OnNext {1} on theradId: {0}", Thread.CurrentThread.ManagedThreadId, obj);
                subject.OnNext(obj);
            };

            notify(1);
            new Thread(notify).Start(2);
            new Thread(notify).Start(3);
        }

        public void Blocking()
        {
            //First is blocking
            var firstItem = GetTemperature().First();
            Console.WriteLine(firstItem);

            GetTemperature().Take(1).Subscribe(
                Console.WriteLine);
        }

        public IObservable<int> GetTemperature()
        {
            return Observable.Create<int>(
            o =>
            {
                o.OnNext(27);
                o.OnNext(26);
                o.OnNext(24);
                return () => { };
            }).SubscribeOn(NewThreadScheduler.Default);
        }

        public void PassDataInSchedule()
        {
            var myName = "Robi";
            NewThreadScheduler.Default.Schedule(myName,
                (_, state) =>
                {
                    Console.WriteLine(state);
                    return Disposable.Empty;
                });
            myName = "John";
        }

        public void ScheduleWithDateTimeOffset()
        {
            var delay = TimeSpan.FromSeconds(1);
            Console.WriteLine("Before schedule at{O:o", DateTime.Now);

            NewThreadScheduler.Default.Schedule(delay,
            () => Console.WriteLine("Inside schedule at{O:o", DateTime.Now));

            Console.WriteLine("After schedule at{O:o", DateTime.Now);
        }

        public void CancelScedule()
        {
            var delay = TimeSpan.FromSeconds(1);
            Console.WriteLine("Before schedule at{O:o", DateTime.Now);

            var token = NewThreadScheduler.Default.Schedule(delay,
            () => Console.WriteLine("Inside schedule at{O:o", DateTime.Now));

            Console.WriteLine("After schedule at{O:o", DateTime.Now);
            token.Dispose();
        }
    }
}
