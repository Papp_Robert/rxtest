﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Threading;

namespace RxConcurrency
{
    class AdvancedSubscribers
    {
        private static void ScheduleTasks(IScheduler scheduler)
        {
            Action leafAction = () => Console.WriteLine("-------leafAction");

            Action innerAction = () =>
            {
                Console.WriteLine("-- inner action start");
                scheduler.Schedule(leafAction);
                Console.WriteLine("-- inner action end");
            };

            Action outerAction = () =>
            {
                Console.WriteLine("outer action start");
                scheduler.Schedule(innerAction);
                Console.WriteLine("outer action end");
            };
            scheduler.Schedule(outerAction);
        }

        public void CurrentThreadExample()
        {
            ScheduleTasks(Scheduler.CurrentThread);
        }

        public void ImmediateExample()
        {
            ScheduleTasks(Scheduler.Immediate);
        }

        public static IDisposable OuterAction(IScheduler scheduler, string state)
        {
            Console.WriteLine("{0} start. ThreadId:{1}", state, Thread.CurrentThread.ManagedThreadId);
            scheduler.Schedule(state + ".inner", InnerAction);

            Console.WriteLine("{0} end. ThreadId: {1}", state, Thread.CurrentThread.ManagedThreadId);
            return Disposable.Empty;
        }

        public static IDisposable InnerAction(IScheduler scheduler, string state)
        {
            Console.WriteLine("{0} start. ThreadId:{1}", state, Thread.CurrentThread.ManagedThreadId);
            scheduler.Schedule(state + ".Leaf", LeafAction);

            Console.WriteLine("{0} end. ThreadId: {1}", state, Thread.CurrentThread.ManagedThreadId);
            return Disposable.Empty;
        }

        public static IDisposable LeafAction(IScheduler scheduler, string state)
        {
            Console.WriteLine("{0} ThreadId:{1}", state, Thread.CurrentThread.ManagedThreadId);
            return Disposable.Empty;
        }


        public void NewThreadScheduler()
        {
            Console.WriteLine("Starting on thread :{0}", Thread.CurrentThread.ManagedThreadId);
            Scheduler.NewThread.Schedule("A", OuterAction);
            Scheduler.NewThread.Schedule("B", OuterAction);
        }

        public void ThreadPool()
        {
            Console.WriteLine("Starting on thread :{0}", Thread.CurrentThread.ManagedThreadId);
            Scheduler.ThreadPool.Schedule("A", OuterAction);
            Scheduler.ThreadPool.Schedule("B", OuterAction);
        }

        public void TaskPool()
        {
            Console.WriteLine("Starting on thread :{0}", Thread.CurrentThread.ManagedThreadId);
            Scheduler.TaskPool.Schedule("A", OuterAction);
            Scheduler.TaskPool.Schedule("B", OuterAction);
        }

        public void TaskPoolSchedulerExample()
        {
            Console.WriteLine("Starting on thread :{0}", Thread.CurrentThread.ManagedThreadId);
            TaskPoolScheduler.Default.Schedule("A", OuterAction);
            TaskPoolScheduler.Default.Schedule("B", OuterAction);
        }
    }
}
