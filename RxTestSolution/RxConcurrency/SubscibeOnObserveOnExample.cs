﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;

namespace RxConcurrency
{
    class SubscibeOnObserveOnExample
    {
        public void WithoutSubscribeOn()
        {
            Console.WriteLine("Starting on threadId: {0}", Thread.CurrentThread.ManagedThreadId);

            var source = Observable.Create<int>(
                o =>
                {
                    Console.WriteLine("Invoked on threadId: {0}", Thread.CurrentThread.ManagedThreadId);
                    o.OnNext(1);
                    o.OnNext(2);
                    o.OnNext(3);
                    o.OnCompleted();

                    Console.WriteLine("Finished on threadId: {0}", Thread.CurrentThread.ManagedThreadId);
                    return Disposable.Empty;
                });

            source.Subscribe(
                 o => Console.WriteLine("Received {1} on threadId: {0}", Thread.CurrentThread.ManagedThreadId, o),
                 () => Console.WriteLine("OnCompleted on theradId {0}", Thread.CurrentThread.ManagedThreadId));

            Console.WriteLine("Subscribed on theradId {0}", Thread.CurrentThread.ManagedThreadId);
        }

        public void SubscribeOn()
        {
            Console.WriteLine("Starting on threadId: {0}", Thread.CurrentThread.ManagedThreadId);

            var source = Observable.Create<int>(
                o =>
                {
                    Console.WriteLine("Invoked on threadId: {0}", Thread.CurrentThread.ManagedThreadId);
                    o.OnNext(10);
                    o.OnNext(22);
                    o.OnNext(30);
                    o.OnCompleted();

                    Console.WriteLine("Finished on threadId: {0}", Thread.CurrentThread.ManagedThreadId);
                    return Disposable.Empty;
                });

            source
                .SubscribeOn(NewThreadScheduler.Default)
                .Subscribe(
                 o => Console.WriteLine("Received {1} on threadId: {0}", Thread.CurrentThread.ManagedThreadId, o),
                 () => Console.WriteLine("OnCompleted on theradId {0}", Thread.CurrentThread.ManagedThreadId));

            Console.WriteLine("Subscribed on theradId {0}", Thread.CurrentThread.ManagedThreadId);
        }

        public void SubscribeOnWithObserveOn()
        {
            Console.WriteLine("Starting on threadId: {0}", Thread.CurrentThread.ManagedThreadId);

            var source = Observable.Create<int>(
                o =>
                {
                    Console.WriteLine("Invoked on threadId: {0}", Thread.CurrentThread.ManagedThreadId);
                    o.OnNext(1);
                    o.OnNext(2);
                    o.OnNext(3);
                    o.OnCompleted();

                    Console.WriteLine("Finished on threadId: {0}", Thread.CurrentThread.ManagedThreadId);
                    return Disposable.Empty;
                });

            source
                .SubscribeOn(NewThreadScheduler.Default)
                .ObserveOn(Scheduler.CurrentThread)
                .Subscribe(
                 o => Console.WriteLine("Received {1} on threadId: {0}", Thread.CurrentThread.ManagedThreadId, o),
                 () => Console.WriteLine("OnCompleted on theradId {0}", Thread.CurrentThread.ManagedThreadId));

            Console.WriteLine("Subscribed on theradId {0}", Thread.CurrentThread.ManagedThreadId);
        }
    }
}
