﻿using System;

namespace RxConcurrency
{
    class Program
    {
        static void Main(string[] args)
        {
            SingleThreadExample singleThreadExample = new SingleThreadExample();
            //singleThreadExample.SingleThread();
            //singleThreadExample.Blocking();

            SubscibeOnObserveOnExample subscibeOnObserveOnExample = new SubscibeOnObserveOnExample();
            //subscibeOnObserveOnExample.WithoutSubscribeOn();
            //subscibeOnObserveOnExample.SubscribeOn();

            //Console.WriteLine("*********************************");

            //subscibeOnObserveOnExample.SubscribeOnWithObserveOn();

            AdvancedSubscribers advancedSubscribers = new AdvancedSubscribers();
            //advancedSubscribers.CurrentThreadExample();
            //advancedSubscribers.ImmediateExample() ;
            //advancedSubscribers.NewThreadScheduler();
            //advancedSubscribers.ThreadPool();
            //advancedSubscribers.TaskPool();
            advancedSubscribers.TaskPoolSchedulerExample();

             /*
              UI Applications
                The final subscriber is normally the presentation layer and should control the scheduling.
                Observe on the DispatcherScheduler to allow updating of ViewModels
                Subscribe on a background thread to prevent the UI from becoming unresponsive
                If the subscription will not block for more than 50ms then
                Use the TaskPoolScheduler if available, or
                Use the ThreadPoolScheduler
                If any part of the subscription could block for longer than 50ms, then you should use the NewThreadScheduler.

             Service layer
                If your service is reading data from a queue of some sort, consider using a dedicated EventLoopScheduler. This way, you can preserve order of events
                If processing an item is expensive (>50ms or requires I/O), then consider using a NewThreadScheduler
                If you just need the scheduler for a timer, e.g. for Observable.Interval or Observable.Timer, then favor the TaskPool. Use the ThreadPool if the TaskPool is not available for your platform.
                */


            Console.ReadLine();             
        }
    }
}
