﻿using System;
using System.Collections.Generic;

namespace RxExample.Repository
{
    class FilmRepository
    {
        private Random rnd;
        public FilmRepository()
        {
            rnd = new Random();
        }
        List<Film> films = new List<Film>
        {
            new Film{Title = "Titanic", Length = 194, ReleaseDate = 1997},
            new Film{Title = "Starship Troopers", Length = 129, ReleaseDate = 1997},
            new Film{Title = "Good will hunting", Length = 126, ReleaseDate = 1997},
            new Film{Title = "Vegas vacation", Length = 93, ReleaseDate = 1997},
            new Film{Title = "The shawshank Redemption", Length = 142, ReleaseDate = 1994},
            new Film{Title = "The godfather", Length = 175, ReleaseDate = 1972},
            new Film{Title = "The godfather part II", Length = 202, ReleaseDate = 1974},
            new Film{Title = "The dark night", Length = 152, ReleaseDate = 2008},
            new Film{Title = "12 angry man", Length = 96, ReleaseDate = 1957},
            new Film{Title = "Schindler's list", Length = 195, ReleaseDate = 1993}
        };

        public Film GetFilm ()
        {
                        
            return films[rnd.Next(0, films.Count - 1)];
        }
    }
}
