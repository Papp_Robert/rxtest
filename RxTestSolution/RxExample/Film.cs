﻿namespace RxExample
{
    public class Film
    {
        public string Title { get; set; }

        public int Length { get; set; }

        public int ReleaseDate { get; set; }
    }
}
