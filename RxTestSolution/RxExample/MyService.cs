﻿using RxExample.Repository;
using System;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RxExample
{
    class MyService
    {
        private FilmRepository filmRepository;

        public MyService()
        {
            filmRepository = new FilmRepository();
        }

        public IObservable<Film> GetFilms()
        {
            return Observable.Create<Film>(
                film =>
                {
                    film.OnNext(new Film { Title = "Titanic", ReleaseDate = 1997, Length = 194 });

                    return () => { };
                }
                )
                .SubscribeOnDispatcher();
        }

        public IObservable<Film> GetRandomFilmsOnBackgroundThread()
        {
            var ob = Observable.Create<Film>(film =>
           {
               var cancel = new CancellationDisposable(); // internally creates a new CancellationTokenSource
                NewThreadScheduler.Default.Schedule(() =>
               {
                   for (; ; )
                   {
                       Thread.Sleep(200);  // here we do the long lasting background operation
                        if (!cancel.Token.IsCancellationRequested)    // check cancel token periodically
                            film.OnNext(filmRepository.GetFilm());
                       else
                       {
                           Console.WriteLine("Aborting because cancel event was signaled!");
                           film.OnCompleted();
                           return;
                       }
                   }
               });

               return cancel;
           });
            return ob;
        }

        public IObservable<Film> GetRandomFilms()
        {
            var ob = Observable.Create<Film>(film =>
            {
                for (int i = 0; i < 5; i++)
                {
                    film.OnNext(filmRepository.GetFilm());
                }
                film.OnCompleted();
                return Disposable.Empty;
            });

            return ob;
        }
    }
}

