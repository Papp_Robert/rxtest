﻿using RxExample.Repository;
using System;
using System.Collections.ObjectModel;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using System.Windows;

namespace RxExample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Film> Films { get; private set; }

        private readonly MyService service = new MyService();

        public MainWindow()
        {
            InitializeComponent();

            Films = new ObservableCollection<Film>();
            //Films.Add(new Film { Title = "Titanic", ReleaseDate = 1997, Length = 194 });

            DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Films.Add(service.GetFilms().First());
            //service.GetFilms()
            //    .Subscribe(
            //    f => Films.Add(f)
            //    );
        }

        private void GetFilmsButtonClick(object sender, RoutedEventArgs e)
        {
               IDisposable subscription = service.GetRandomFilms()                
                .SubscribeOn(NewThreadScheduler.Default)
                .Subscribe(
                f => Films.Add(f));
        }

        private void GetFilmsOnBackGroundThreadButtonClick(object sender, RoutedEventArgs e)
        {
            IDisposable subscription = service.GetRandomFilmsOnBackgroundThread()
             .ObserveOn(Application.Current.Dispatcher)
             .Subscribe(
             f => Films.Add(f));

            subscription.Dispose();
        }
    }
}
