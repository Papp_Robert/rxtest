﻿using System;

namespace ObserverPattern
{
    class ConcreteObserver : Observer
    {
        private string name;
        private string observerState;

        // Constructor

        public ConcreteObserver(
          ConcreteSubject subject, string name)
        {
            this.Subject = subject;
            this.name = name;
        }

        public override void Update()
        {
            observerState = Subject.SubjectState;
            Console.WriteLine("Observer {0}'s new state is {1}",
              name, observerState);
        }

        // Gets or sets subject

        public ConcreteSubject Subject { get; set; }
    }
}
