﻿namespace ObserverPattern
{
    class ConcreteSubject : Subject
    {
        // Gets or sets subject state
        public string SubjectState { get; set; }
    }
}
