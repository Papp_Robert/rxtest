﻿//Link to prezi: https://prezi.com/view/Rg5egDq0WT9uxWlhGHNv/

using System;

namespace ObserverPattern
{
    class Program
    {
        static void Main(string[] args)
        { 
            // Configure Observer pattern
            ConcreteSubject s = new ConcreteSubject();

            s.Attach(new ConcreteObserver(s, "X"));
            s.Attach(new ConcreteObserver(s, "Y"));
            s.Attach(new ConcreteObserver(s, "Z"));

            // Change subject and notify observers
            s.SubjectState = "ABC";
            s.Notify();

            // Wait for user
            Console.ReadKey();
        }
    }
}
