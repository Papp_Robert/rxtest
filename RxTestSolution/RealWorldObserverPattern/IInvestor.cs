﻿namespace RealWorldObserverPattern
{
    //The observer
    public interface IInvestor
    {
        void Update(Stock stock);
    }
}
