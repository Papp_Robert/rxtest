﻿using Microsoft.Reactive.Testing;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace RxUnitTestExamples
{
    [TestFixture]
    public class RxTestWithTestScheduler
    {
        [Test]
        public void FirstTest()
        {
            var scheduler = new TestScheduler();

            var wasExecuted = false;
            scheduler.Schedule(
                () => wasExecuted = true);

            Assert.IsFalse(wasExecuted);
            scheduler.AdvanceBy(1);

            Assert.IsTrue(wasExecuted);
        }

        [Test]
        public void AdvanceTestToExample()
        {
            var scheduler = new TestScheduler();
            scheduler.Schedule(() => Console.WriteLine("A"));

            scheduler.Schedule(TimeSpan.FromTicks(10), () => Console.WriteLine("B"));
            scheduler.Schedule(TimeSpan.FromTicks(20), () => Console.WriteLine("C"));

            TestContext.Out.WriteLine("scheduler.AdvanceTo(1)");
            scheduler.AdvanceTo(1);

            TestContext.Out.WriteLine("scheduler.AdvanceTo(10)");
            scheduler.AdvanceTo(10);

            TestContext.Out.WriteLine("scheduler.AdvanceTo(15)");
            scheduler.AdvanceTo(15);

            TestContext.Out.WriteLine("scheduler.AdvanceTo(20)");
            scheduler.AdvanceTo(20);
        }

        [Test]
        public void AdvanceByTestExample()
        {
            var scheduler = new TestScheduler();
            scheduler.Schedule(() => Console.WriteLine("A"));

            scheduler.Schedule(TimeSpan.FromTicks(10), () => Console.WriteLine("B"));
            scheduler.Schedule(TimeSpan.FromTicks(20), () => Console.WriteLine("C"));

            TestContext.Out.WriteLine("scheduler.AdvanceBy(1)");
            scheduler.AdvanceBy(1);

            TestContext.Out.WriteLine("scheduler.AdvanceBy(9)");
            scheduler.AdvanceBy(9);

            TestContext.Out.WriteLine("scheduler.AdvanceBy(5)");
            scheduler.AdvanceBy(5);

            TestContext.Out.WriteLine("scheduler.AdvanceBy(5)");
            scheduler.AdvanceBy(5);
        }

        [Test]
        public void StartTestExample()
        {
            var scheduler = new TestScheduler();
            scheduler.Schedule(() => Console.WriteLine("A"));

            scheduler.Schedule(TimeSpan.FromTicks(10), () => Console.WriteLine("B"));
            scheduler.Schedule(TimeSpan.FromTicks(20), () => Console.WriteLine("C"));

            TestContext.Out.WriteLine("scheduler.Start()");
            scheduler.Start();
            TestContext.Out.WriteLine("scheduler.Clock:{0}", scheduler.Clock);
        }

        [Test]
        public void StartTestExample2()
        {
            var scheduler = new TestScheduler();
            scheduler.Schedule(() => Console.WriteLine("A"));

            scheduler.Schedule(TimeSpan.FromTicks(10), () => Console.WriteLine("B"));
            scheduler.Schedule(TimeSpan.FromTicks(20), () => Console.WriteLine("C"));

            TestContext.Out.WriteLine("scheduler.Start()");
            scheduler.Start();
            TestContext.Out.WriteLine("scheduler.Clock:{0}", scheduler.Clock);
            scheduler.Schedule(() => Console.WriteLine("D"));
        }

        [Test]
        public void StopTestExample()
        {
            var scheduler = new TestScheduler();
            scheduler.Schedule(() => Console.WriteLine("A"));

            scheduler.Schedule(TimeSpan.FromTicks(10), () => Console.WriteLine("B"));
            scheduler.Schedule(TimeSpan.FromTicks(15), scheduler.Stop);        
            scheduler.Schedule(TimeSpan.FromTicks(20), () => Console.WriteLine("C"));

            TestContext.Out.WriteLine("scheduler.Start()");
            scheduler.Start();

            TestContext.Out.WriteLine("scheduler.Clock:{0}", scheduler.Clock);
        }

        [Test]
        public void AutomaticAdvancedToTest()
        {
            var scheduler = new TestScheduler();

            scheduler.Schedule(TimeSpan.FromTicks(10), () => Console.WriteLine("A"));
            scheduler.Schedule(TimeSpan.FromTicks(10), () => Console.WriteLine("B"));
            scheduler.Schedule(TimeSpan.FromTicks(10), () => Console.WriteLine("C"));

            TestContext.Out.WriteLine("scheduler.Start()");
            scheduler.Start();

            TestContext.Out.WriteLine("scheduler.Clock:{0}", scheduler.Clock);
        }

        [Test]
        public void RealTest()
        {
            var expectedValues = new long[] { 0, 1, 2, 3, 4 };
            var actualValues = new List<long>();

            var scheduler = new TestScheduler();
            var interval = Observable.Interval(TimeSpan.FromSeconds(1), scheduler).Take(5);
            interval.Subscribe(actualValues.Add);

            scheduler.Start();
            CollectionAssert.AreEqual(expectedValues, actualValues);
        }

        [Test]
        public void AdvancedStart()
        {
            var scheduler = new TestScheduler();
            var source = Observable.Interval(TimeSpan.FromSeconds(1), scheduler).Take(4);
            var testObserver = scheduler.Start(
                () => source,
                0,
                0,
                TimeSpan.FromSeconds(5).Ticks
                );

            TestContext.Out.WriteLine("Time is {0} ticks", scheduler.Clock);
            TestContext.Out.WriteLine("Received {0} notifications", testObserver.Messages.Count);

            foreach (var message in testObserver.Messages)
            {
                TestContext.Out.WriteLine("{0} : {1}", message.Value, message.Time);
            }
        }

        //Hot observables are ones that are pushing even when you are not subscribed to the observable. 
        //Like mouse moves, or Timer ticks or anything like that. Cold observables are ones that start pushing only 
        //when you subscribe, and they start over if you subscribe again.

        [Test]
        public void CreateColdObservable()
        {
            var scheduler = new TestScheduler();
            var source = scheduler.CreateColdObservable(
                new Recorded<Notification<long>>(10000000, Notification.CreateOnNext(0L)),
                new Recorded<Notification<long>>(20000000, Notification.CreateOnNext(2L)),
                new Recorded<Notification<long>>(30000000, Notification.CreateOnNext(3L)),
                new Recorded<Notification<long>>(40000000, Notification.CreateOnCompleted<long>()));

            var testObserver = scheduler.Start(
                () => source,
                0,
                TimeSpan.FromSeconds(1).Ticks,
                TimeSpan.FromSeconds(5).Ticks
                );

            TestContext.Out.WriteLine("Time is {0} ticks", scheduler.Clock);
            TestContext.Out.WriteLine("Received {0} notifications", testObserver.Messages.Count);

            foreach (var message in testObserver.Messages)
            {
                TestContext.Out.WriteLine("{0} : {1}", message.Value, message.Time);
            }
        }

        [Test]
        public void CreateHotObservable()
        {
            var scheduler = new TestScheduler();
            var source = scheduler.CreateHotObservable(
                new Recorded<Notification<long>>(10000000, Notification.CreateOnNext(0L)),
                new Recorded<Notification<long>>(20000000, Notification.CreateOnNext(2L)),
                new Recorded<Notification<long>>(30000000, Notification.CreateOnNext(3L)),
                new Recorded<Notification<long>>(40000000, Notification.CreateOnCompleted<long>()));

            var testObserver = scheduler.Start(
                () => source,
                0,
                TimeSpan.FromSeconds(1).Ticks,
                TimeSpan.FromSeconds(5).Ticks
                );

            TestContext.Out.WriteLine("Time is {0} ticks", scheduler.Clock);
            TestContext.Out.WriteLine("Received {0} notifications", testObserver.Messages.Count);

            foreach (var message in testObserver.Messages)
            {
                TestContext.Out.WriteLine("{0} : {1}", message.Value, message.Time);
            }
        }
    }
}
