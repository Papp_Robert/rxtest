﻿using System.Reactive.Concurrency;

namespace RxUnitTest
{
    public interface ISchedulerProvider
    {
        IScheduler CurrectThread { get; }
        IScheduler Dispatcher { get; }
        IScheduler Immediate { get; }
        IScheduler NewThread { get; }

        IScheduler ThreadPool { get; }
    }
}
