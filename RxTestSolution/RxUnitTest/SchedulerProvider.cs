﻿

using System.Reactive.Concurrency;

namespace RxUnitTest
{
    public sealed class SchedulerProvider : ISchedulerProvider
    {
        public IScheduler CurrectThread => Scheduler.CurrentThread;

        public IScheduler Dispatcher => DispatcherScheduler.Instance;

        public IScheduler Immediate => Scheduler.Immediate;

        public IScheduler NewThread => Scheduler.NewThread;

        public IScheduler ThreadPool => Scheduler.ThreadPool;
    }
}
